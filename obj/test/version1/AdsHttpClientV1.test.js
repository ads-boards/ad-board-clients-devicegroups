"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_ads_node_1 = require("ad-board-services-devicegroups-node");
const ad_board_services_ads_node_2 = require("ad-board-services-devicegroups-node");
const ad_board_services_ads_node_3 = require("ad-board-services-devicegroups-node");
const DevicegroupsHttpClientV1_1 = require("../../src/version1/DevicegroupsHttpClientV1");
const DevicegroupsClientFixtureV1_1 = require("./DevicegroupsClientFixtureV1");
var httpConfig = pip_services3_commons_node_2.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
suite('DevicegroupsRestClientV1', () => {
    let service;
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_ads_node_1.DevicegroupsMemoryPersistence();
        let controller = new ad_board_services_ads_node_2.DevicegroupsController();
        service = new ad_board_services_ads_node_3.DevicegroupsHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        client = new DevicegroupsHttpClientV1_1.DevicegroupsHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);
        fixture = new DevicegroupsClientFixtureV1_1.DevicegroupsClientFixtureV1(client);
        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=DevicegroupsHttpClientV1.test.js.map