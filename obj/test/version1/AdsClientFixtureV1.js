"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsClientFixtureV1 = void 0;
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
let AD1 = {
    id: '1',
    publish_group_ids: ['1', '2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img_url: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    create_time: new Date(),
    end_time: new Date()
};
let AD2 = {
    id: '2',
    publish_group_ids: ['1', '3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img_url: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    create_time: new Date(),
    end_time: new Date()
};
class DevicegroupsClientFixtureV1 {
    constructor(client) {
        this._client = client;
    }
    testCrudOperations(done) {
        let ad1, ad2;
        async.series([
            // Create one ad
            (callback) => {
                this._client.createAd(null, AD1, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    //assert.equal(ad.name.get('en'), AD1.name.get('en'));
                    assert.equal(ad.id, AD1.id);
                    assert.equal(ad.title, AD1.title);
                    assert.equal(ad.text, AD1.text);
                    ad1 = ad;
                    callback();
                });
            },
            // Create another ad
            (callback) => {
                this._client.createAd(null, AD2, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.id, AD2.id);
                    assert.equal(ad.title, AD2.title);
                    assert.equal(ad.text, AD2.text);
                    ad2 = ad;
                    callback();
                });
            },
            // Get all ads
            (callback) => {
                this._client.getDevicegroups(null, null, new pip_services3_commons_node_1.PagingParams(0, 5, false), (err, ads) => {
                    assert.isNull(err);
                    assert.isObject(ads);
                    assert.isTrue(ads.data.length >= 2);
                    callback();
                });
            },
            // Update the ad
            (callback) => {
                ad1.text = 'Updated Name 1';
                this._client.updateAd(null, ad1, (err, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.text, 'Updated Name 1');
                    assert.equal(ad.id, AD1.id);
                    ad1 = ad;
                    callback();
                });
            },
            // Delete ad
            (callback) => {
                this._client.deleteAdById(null, ad1.id, (err) => {
                    assert.isNull(err);
                    callback();
                });
            },
            // Try to get delete ad
            (callback) => {
                this._client.getAdById(null, ad1.id, (err, ad) => {
                    assert.isNull(err);
                    assert.isNull(ad || null);
                    callback();
                });
            }
        ], done);
    }
}
exports.DevicegroupsClientFixtureV1 = DevicegroupsClientFixtureV1;
//# sourceMappingURL=DevicegroupsClientFixtureV1.js.map