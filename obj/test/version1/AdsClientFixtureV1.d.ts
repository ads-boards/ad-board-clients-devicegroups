import { IDevicegroupsClientV1 } from '../../src/version1/IDevicegroupsClientV1';
export declare class DevicegroupsClientFixtureV1 {
    private _client;
    constructor(client: IDevicegroupsClientV1);
    testCrudOperations(done: any): void;
}
