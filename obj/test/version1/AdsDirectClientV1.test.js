"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_ads_node_1 = require("ad-board-services-devicegroups-node");
const ad_board_services_ads_node_2 = require("ad-board-services-devicegroups-node");
const DevicegroupsDirectClientV1_1 = require("../../src/version1/DevicegroupsDirectClientV1");
const DevicegroupsClientFixtureV1_1 = require("./DevicegroupsClientFixtureV1");
suite('DevicegroupsDirectClientV1', () => {
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_ads_node_1.DevicegroupsMemoryPersistence();
        let controller = new ad_board_services_ads_node_2.DevicegroupsController();
        let references = pip_services3_commons_node_2.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '1.0'), controller);
        controller.setReferences(references);
        client = new DevicegroupsDirectClientV1_1.DevicegroupsDirectClientV1();
        client.setReferences(references);
        fixture = new DevicegroupsClientFixtureV1_1.DevicegroupsClientFixtureV1(client);
        client.open(null, done);
    });
    suiteTeardown((done) => {
        client.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=DevicegroupsDirectClientV1.test.js.map