"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsClientFactory = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const DevicegroupsNullClientV1_1 = require("../version1/DevicegroupsNullClientV1");
const DevicegroupsDirectClientV1_1 = require("../version1/DevicegroupsDirectClientV1");
const DevicegroupsHttpClientV1_1 = require("../version1/DevicegroupsHttpClientV1");
class DevicegroupsClientFactory extends pip_services3_components_node_1.Factory {
    constructor() {
        super();
        this.registerAsType(DevicegroupsClientFactory.NullClientV1Descriptor, DevicegroupsNullClientV1_1.DevicegroupsNullClientV1);
        this.registerAsType(DevicegroupsClientFactory.DirectClientV1Descriptor, DevicegroupsDirectClientV1_1.DevicegroupsDirectClientV1);
        this.registerAsType(DevicegroupsClientFactory.HttpClientV1Descriptor, DevicegroupsHttpClientV1_1.DevicegroupsHttpClientV1);
    }
}
exports.DevicegroupsClientFactory = DevicegroupsClientFactory;
DevicegroupsClientFactory.Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'factory', 'default', 'default', '1.0');
DevicegroupsClientFactory.NullClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'client', 'null', 'default', '1.0');
DevicegroupsClientFactory.DirectClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'client', 'direct', 'default', '1.0');
DevicegroupsClientFactory.HttpClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'client', 'http', 'default', '1.0');
//# sourceMappingURL=DevicegroupsClientFactory.js.map