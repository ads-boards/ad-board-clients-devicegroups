import { IStringIdentifiable } from 'pip-services3-commons-node';
export declare class DevicegroupV1 implements IStringIdentifiable {
    id: string;
    publish_group_ids: string[];
    title: string;
    text: string;
    img_url: string;
    text_color: string;
    text_size: number;
    background_color: string;
    critical: boolean;
    deleted: boolean;
    create_time: Date;
    end_time?: Date;
}
