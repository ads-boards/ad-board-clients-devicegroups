import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IDevicegroupsClientV1 } from './IDevicegroupsClientV1';
import { DevicegroupV1 } from './DevicegroupV1';
export declare class DevicegroupsNullClientV1 implements IDevicegroupsClientV1 {
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
    createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    deleteDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
}
