import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IDevicegroupsClientV1 } from './IDevicegroupsClientV1';
import { DevicegroupV1 } from './DevicegroupV1';
export declare class DevicegroupsNullClientV1 implements IDevicegroupsClientV1 {
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getAdById(correlationId: string, adId: string, callback: (err: any, ad: DevicegroupV1) => void): void;
    createAd(correlationId: string, ad: DevicegroupV1, callback: (err: any, ad: DevicegroupV1) => void): void;
    updateAd(correlationId: string, ad: DevicegroupV1, callback: (err: any, ad: DevicegroupV1) => void): void;
    deleteAdById(correlationId: string, adId: string, callback: (err: any, ad: DevicegroupV1) => void): void;
}
