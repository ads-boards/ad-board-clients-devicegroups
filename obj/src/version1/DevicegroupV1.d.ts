import { IStringIdentifiable } from 'pip-services3-commons-node';
export declare class DevicegroupV1 implements IStringIdentifiable {
    id: string;
    device_ids: string[];
    description: string;
}
