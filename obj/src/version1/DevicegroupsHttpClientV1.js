"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsHttpClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicegroupsHttpClientV1 extends pip_services3_rpc_node_1.CommandableHttpClient {
    constructor(config) {
        super('v1/devicegroups');
        if (config != null)
            this.configure(pip_services3_commons_node_1.ConfigParams.fromValue(config));
    }
    getDevicegroups(correlationId, filter, paging, callback) {
        this.callCommand('get_devicegroups', correlationId, {
            filter: filter,
            paging: paging
        }, callback);
    }
    getDevicegroupById(correlationId, devicegroupId, callback) {
        this.callCommand('get_devicegroup_by_id', correlationId, {
            devicegroup_id: devicegroupId
        }, callback);
    }
    createDevicegroup(correlationId, devicegroup, callback) {
        this.callCommand('create_devicegroup', correlationId, {
            devicegroup: devicegroup
        }, callback);
    }
    updateDevicegroup(correlationId, devicegroup, callback) {
        this.callCommand('update_devicegroup', correlationId, {
            devicegroup: devicegroup
        }, callback);
    }
    deleteDevicegroupById(correlationId, devicegroupId, callback) {
        this.callCommand('delete_devicegroup_by_id', correlationId, {
            devicegroup_id: devicegroupId
        }, callback);
    }
}
exports.DevicegroupsHttpClientV1 = DevicegroupsHttpClientV1;
//# sourceMappingURL=DevicegroupsHttpClientV1.js.map