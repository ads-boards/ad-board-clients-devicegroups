import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { CommandableHttpClient } from 'pip-services3-rpc-node';
import { DevicegroupV1 } from './DevicegroupV1';
import { IDevicegroupsClientV1 } from './IDevicegroupsClientV1';
export declare class DevicegroupsHttpClientV1 extends CommandableHttpClient implements IDevicegroupsClientV1 {
    constructor(config?: any);
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
    createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    deleteDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
}
