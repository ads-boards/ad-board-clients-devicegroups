"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsNullClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
class DevicegroupsNullClientV1 {
    getDevicegroups(correlationId, filter, paging, callback) {
        callback(null, new pip_services3_commons_node_1.DataPage([], 0));
    }
    getAdById(correlationId, adId, callback) {
        callback(null, null);
    }
    createAd(correlationId, ad, callback) {
        callback(null, ad);
    }
    updateAd(correlationId, ad, callback) {
        callback(null, ad);
    }
    deleteAdById(correlationId, adId, callback) {
        callback(null, null);
    }
}
exports.DevicegroupsNullClientV1 = DevicegroupsNullClientV1;
//# sourceMappingURL=DevicegroupsNullClientV1.js.map