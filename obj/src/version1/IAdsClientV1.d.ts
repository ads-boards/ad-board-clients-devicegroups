import { DataPage } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DevicegroupV1 } from './DevicegroupV1';
export interface IDevicegroupsClientV1 {
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getAdById(correlationId: string, ad_id: string, callback: (err: any, ad: DevicegroupV1) => void): void;
    createAd(correlationId: string, ad: DevicegroupV1, callback: (err: any, ad: DevicegroupV1) => void): void;
    updateAd(correlationId: string, ad: DevicegroupV1, callback: (err: any, ad: DevicegroupV1) => void): void;
    deleteAdById(correlationId: string, ad_id: string, callback: (err: any, ad: DevicegroupV1) => void): void;
}
