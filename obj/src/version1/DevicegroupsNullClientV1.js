"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsNullClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
class DevicegroupsNullClientV1 {
    getDevicegroups(correlationId, filter, paging, callback) {
        callback(null, new pip_services3_commons_node_1.DataPage([], 0));
    }
    getDevicegroupById(correlationId, devicegroupId, callback) {
        callback(null, null);
    }
    createDevicegroup(correlationId, devicegroup, callback) {
        callback(null, devicegroup);
    }
    updateDevicegroup(correlationId, devicegroup, callback) {
        callback(null, devicegroup);
    }
    deleteDevicegroupById(correlationId, devicegroupId, callback) {
        callback(null, null);
    }
}
exports.DevicegroupsNullClientV1 = DevicegroupsNullClientV1;
//# sourceMappingURL=DevicegroupsNullClientV1.js.map