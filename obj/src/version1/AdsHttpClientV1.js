"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsHttpClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicegroupsHttpClientV1 extends pip_services3_rpc_node_1.CommandableHttpClient {
    constructor(config) {
        super('v1/ads');
        if (config != null)
            this.configure(pip_services3_commons_node_1.ConfigParams.fromValue(config));
    }
    getDevicegroups(correlationId, filter, paging, callback) {
        this.callCommand('get_ads', correlationId, {
            filter: filter,
            paging: paging
        }, callback);
    }
    getAdById(correlationId, adId, callback) {
        this.callCommand('get_ad_by_id', correlationId, {
            ad_id: adId
        }, callback);
    }
    createAd(correlationId, ad, callback) {
        this.callCommand('create_ad', correlationId, {
            ad: ad
        }, callback);
    }
    updateAd(correlationId, ad, callback) {
        this.callCommand('update_ad', correlationId, {
            ad: ad
        }, callback);
    }
    deleteAdById(correlationId, adId, callback) {
        this.callCommand('delete_ad_by_id', correlationId, {
            ad_id: adId
        }, callback);
    }
}
exports.DevicegroupsHttpClientV1 = DevicegroupsHttpClientV1;
//# sourceMappingURL=DevicegroupsHttpClientV1.js.map