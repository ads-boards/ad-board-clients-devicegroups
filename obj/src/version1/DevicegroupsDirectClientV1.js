"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsDirectClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicegroupsDirectClientV1 extends pip_services3_rpc_node_1.DirectClient {
    constructor() {
        super();
        this._dependencyResolver.put('controller', new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "controller", "*", "*", "*"));
    }
    getDevicegroups(correlationId, filter, paging, callback) {
        let timing = this.instrument(correlationId, 'devicegroups.get_devicegroups');
        this._controller.getDevicegroups(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }
    getDevicegroupById(correlationId, devicegroupId, callback) {
        let timing = this.instrument(correlationId, 'devicegroups.get_devicegroup_by_id');
        this._controller.getDevicegroupById(correlationId, devicegroupId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    createDevicegroup(correlationId, devicegroup, callback) {
        let timing = this.instrument(correlationId, 'devicegroups.create_devicegroup');
        this._controller.createDevicegroup(correlationId, devicegroup, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    updateDevicegroup(correlationId, devicegroup, callback) {
        let timing = this.instrument(correlationId, 'devicegroups.update_devicegroup');
        this._controller.updateDevicegroup(correlationId, devicegroup, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    deleteDevicegroupById(correlationId, devicegroupId, callback) {
        let timing = this.instrument(correlationId, 'devicegroups.delete_devicegroup_by_id');
        this._controller.deleteDevicegroupById(correlationId, devicegroupId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
}
exports.DevicegroupsDirectClientV1 = DevicegroupsDirectClientV1;
//# sourceMappingURL=DevicegroupsDirectClientV1.js.map