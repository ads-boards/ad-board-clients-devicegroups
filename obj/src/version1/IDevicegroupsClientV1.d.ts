import { DataPage } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DevicegroupV1 } from './DevicegroupV1';
export interface IDevicegroupsClientV1 {
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
    createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, item: DevicegroupV1) => void): void;
    deleteDevicegroupById(correlationId: string, devicegroupId: string, callback: (err: any, item: DevicegroupV1) => void): void;
}
