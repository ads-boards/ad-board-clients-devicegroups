import { Descriptor } from 'pip-services3-commons-node';
import { Factory } from 'pip-services3-components-node';

import { DevicegroupsNullClientV1 } from '../version1/DevicegroupsNullClientV1';
import { DevicegroupsDirectClientV1 } from '../version1/DevicegroupsDirectClientV1';
import { DevicegroupsHttpClientV1 } from '../version1/DevicegroupsHttpClientV1';


export class DevicegroupsClientFactory extends Factory {
	public static Descriptor: Descriptor = new Descriptor('ad-board-devicegroups', 'factory', 'default', 'default', '1.0');
	public static NullClientV1Descriptor = new Descriptor('ad-board-devicegroups', 'client', 'null', 'default', '1.0');
	public static DirectClientV1Descriptor = new Descriptor('ad-board-devicegroups', 'client', 'direct', 'default', '1.0');
	public static HttpClientV1Descriptor = new Descriptor('ad-board-devicegroups', 'client', 'http', 'default', '1.0');

	
	constructor() {
		super();

		this.registerAsType(DevicegroupsClientFactory.NullClientV1Descriptor, DevicegroupsNullClientV1);
		this.registerAsType(DevicegroupsClientFactory.DirectClientV1Descriptor, DevicegroupsDirectClientV1);
		this.registerAsType(DevicegroupsClientFactory.HttpClientV1Descriptor, DevicegroupsHttpClientV1);
	}
	
}
