import { ConfigParams } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { CommandableHttpClient } from 'pip-services3-rpc-node';

import { DevicegroupV1 } from './DevicegroupV1';
import { IDevicegroupsClientV1 } from './IDevicegroupsClientV1';

export class DevicegroupsHttpClientV1 extends CommandableHttpClient implements IDevicegroupsClientV1 {       
    
    constructor(config?: any) {
        super('v1/devicegroups');

        if (config != null)
            this.configure(ConfigParams.fromValue(config));
    }
                
    public getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void {
        this.callCommand( 
            'get_devicegroups', 
            correlationId,
            {
                filter: filter,
                paging: paging
            }, 
            callback
        );
    }

    public getDevicegroupById(correlationId: string, devicegroupId: string,
        callback: (err: any, item: DevicegroupV1) => void): void {
        this.callCommand( 
            'get_devicegroup_by_id',
            correlationId,
            {
                devicegroup_id: devicegroupId
            }, 
            callback
        );        
    }

    public createDevicegroup(correlationId: string, devicegroup: DevicegroupV1,
        callback: (err: any, item: DevicegroupV1) => void): void {
        this.callCommand(
            'create_devicegroup',
            correlationId,
            {
                devicegroup: devicegroup
            }, 
            callback
        );
    }

    public updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1,
        callback: (err: any, item: DevicegroupV1) => void): void {
        this.callCommand(
            'update_devicegroup', 
            correlationId,
            {
                devicegroup: devicegroup
            }, 
            callback
        );
    }

    public deleteDevicegroupById(correlationId: string, devicegroupId: string,
        callback: (err: any, item: DevicegroupV1) => void): void {
        this.callCommand(
            'delete_devicegroup_by_id', 
            correlationId,
            {
                devicegroup_id: devicegroupId
            }, 
            callback
        );
    }
    
}
