
import { IStringIdentifiable } from 'pip-services3-commons-node';

export class DevicegroupV1 implements IStringIdentifiable {
    public id: string; // group id
    public device_ids: string[]; // device ids ib this group
    public description: string; // device group description

}