import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams} from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { DirectClient } from 'pip-services3-rpc-node';

import { IDevicegroupsClientV1 } from './IDevicegroupsClientV1';

import { DevicegroupV1 } from './DevicegroupV1';

export class DevicegroupsDirectClientV1 extends DirectClient<any> implements IDevicegroupsClientV1 {
            
    public constructor() {
        super();
        this._dependencyResolver.put('controller', new Descriptor("ad-board-devicegroups", "controller", "*", "*", "*"))
    }

    public getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void {
        let timing = this.instrument(correlationId, 'devicegroups.get_devicegroups');
        this._controller.getDevicegroups(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }

    public getDevicegroupById(correlationId: string, devicegroupId: string, 
        callback: (err: any, item: DevicegroupV1) => void): void {
        let timing = this.instrument(correlationId, 'devicegroups.get_devicegroup_by_id');
        this._controller.getDevicegroupById(correlationId, devicegroupId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, 
        callback: (err: any, item: DevicegroupV1) => void): void {
        let timing = this.instrument(correlationId, 'devicegroups.create_devicegroup');
        this._controller.createDevicegroup(correlationId, devicegroup, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, 
        callback: (err: any, item: DevicegroupV1) => void): void {
        let timing = this.instrument(correlationId, 'devicegroups.update_devicegroup');
        this._controller.updateDevicegroup(correlationId, devicegroup, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public deleteDevicegroupById(correlationId: string, devicegroupId: string,
        callback: (err: any, item: DevicegroupV1) => void): void {
        let timing = this.instrument(correlationId, 'devicegroups.delete_devicegroup_by_id');
        this._controller.deleteDevicegroupById(correlationId, devicegroupId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
}