let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { DevicegroupsMemoryPersistence } from 'ad-board-services-devicegroups-node';
import { DevicegroupsController } from 'ad-board-services-devicegroups-node';
import { DevicegroupsDirectClientV1 } from '../../src/version1/DevicegroupsDirectClientV1';
import { DevicegroupsClientFixtureV1 } from './DevicegroupsClientFixtureV1';

suite('DevicegroupsDirectClientV1', ()=> {
    let client: DevicegroupsDirectClientV1;
    let fixture: DevicegroupsClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new DevicegroupsMemoryPersistence();
        let controller = new DevicegroupsController();

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '1.0'), controller,
        );
        controller.setReferences(references);

        client = new DevicegroupsDirectClientV1();
        client.setReferences(references);

        fixture = new DevicegroupsClientFixtureV1(client);

        client.open(null, done);
    });
    
    suiteTeardown((done) => {
        client.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
