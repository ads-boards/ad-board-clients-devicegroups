let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { DevicegroupsMemoryPersistence } from 'ad-board-services-devicegroups-node';
import { DevicegroupsController } from 'ad-board-services-devicegroups-node';
import { DevicegroupsHttpServiceV1 } from 'ad-board-services-devicegroups-node';
import { DevicegroupsHttpClientV1 } from '../../src/version1/DevicegroupsHttpClientV1';
import { DevicegroupsClientFixtureV1 } from './DevicegroupsClientFixtureV1';

var httpConfig = ConfigParams.fromTuples(
    "connection.protocol", "http",
    "connection.host", "localhost",
    "connection.port", 3000
);

suite('DevicegroupsRestClientV1', ()=> {
    let service: DevicegroupsHttpServiceV1;
    let client: DevicegroupsHttpClientV1;
    let fixture: DevicegroupsClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new DevicegroupsMemoryPersistence();
        let controller = new DevicegroupsController();

        service = new DevicegroupsHttpServiceV1();
        service.configure(httpConfig);

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '1.0'), controller,
            new Descriptor('ad-board-devicegroups', 'service', 'http', 'default', '1.0'), service
        );
        controller.setReferences(references);
        service.setReferences(references);

        client = new DevicegroupsHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);

        fixture = new DevicegroupsClientFixtureV1(client);

        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
