let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { PagingParams, MultiString } from 'pip-services3-commons-node';

import { DevicegroupV1 } from '../../src/version1/DevicegroupV1';
import { IDevicegroupsClientV1 } from '../../src/version1/IDevicegroupsClientV1';

let DEVICEGROUP1: DevicegroupV1 = {
    id: '1',
    device_ids: ['1', '2'],
    description: 'Title 1',

};
let DEVICEGROUP2: DevicegroupV1 = {
    id: '2',
    device_ids: ['1', '3'],
    description: 'Title 2',

};

export class DevicegroupsClientFixtureV1 {
    private _client: IDevicegroupsClientV1;
    
    constructor(client: IDevicegroupsClientV1) {
        this._client = client;
    }
        
    testCrudOperations(done) {
        let devicegroup1, devicegroup2: DevicegroupV1;

        async.series([
        // Create one devicegroup
            (callback) => {
                this._client.createDevicegroup(
                    null,
                    DEVICEGROUP1,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        //assert.equal(devicegroup.name.get('en'), DEVICEGROUP1.name.get('en'));
                        assert.equal(devicegroup.id, DEVICEGROUP1.id);
                        assert.equal(devicegroup.description, DEVICEGROUP1.description);
                        devicegroup1 = devicegroup;

                        callback();
                    }
                );
            },
        // Create another devicegroup
            (callback) => {
                this._client.createDevicegroup(
                    null,
                    DEVICEGROUP2,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.id, DEVICEGROUP2.id);
                        assert.equal(devicegroup.description, DEVICEGROUP2.description);

                        devicegroup2 = devicegroup;

                        callback();
                    }
                );
            },
        // Get all devicegroups
            (callback) => {
                this._client.getDevicegroups(
                    null,
                    null,
                    new PagingParams(0,5,false),
                    (err, devicegroups) => {
                        assert.isNull(err);

                        assert.isObject(devicegroups);
                        assert.isTrue(devicegroups.data.length >= 2);

                        callback();
                    }
                );
            },
        // Update the devicegroup
            (callback) => {
               
                devicegroup1.description = 'Updated Name 1';

                this._client.updateDevicegroup(
                    null,
                    devicegroup1,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.description, 'Updated Name 1');
                        assert.equal(devicegroup.id, DEVICEGROUP1.id);

                        devicegroup1 = devicegroup;

                        callback();
                    }
                );
            },
        // Delete devicegroup
            (callback) => {
                this._client.deleteDevicegroupById(
                    null,
                    devicegroup1.id,
                    (err) => {
                        assert.isNull(err);

                        callback();
                    }
                );
            },
        // Try to get delete devicegroup
            (callback) => {
                this._client.getDevicegroupById(
                    null,
                    devicegroup1.id,
                    (err, devicegroup) => {
                        assert.isNull(err);
                        
                        assert.isNull(devicegroup || null);

                        callback();
                    }
                );
            }
        ], done);
    }
}
